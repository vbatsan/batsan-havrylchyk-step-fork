(function($){
    $(function() {

        $('.navbar-menu__icon').on("click", click);

        function click () {
            if ($(".navbar-menu").css('display') === 'block'){
                $(".navbar-menu").css('display', 'none')
            }else {
                $(".navbar-menu").css('display', 'block')
            }
        }

    });
})(jQuery);