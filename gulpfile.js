const gulp = require("gulp");
//const cache=require('gulp-cache') ;
const sass = require("gulp-sass"),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat'),
    ugli = require('gulp-uglifyjs'),
    cleanCSS = require('gulp-clean-css'),
    autoprefix = require('gulp-autoprefixer'),
    imageMin = require('gulp-imagemin'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    livereload = require('gulp-livereload'),
    octicons = require("gulp-octicons"),
    cache = require('gulp-cache'),
    pngquant = require('gulp-pngquant'),
    clear = require('gulp-clean');


gulp.task('octicons', async function () {
        gulp.src("index.html")
            .pipe(octicons())
            .pipe(gulp.dest('./'))
    });


gulp.task ('sass', function () {
    return gulp.src("src/sass/**/*.scss")
        .pipe(sass().on('error', notify.onError(function () {
            return '<%= error.message %>'
        })))
        .pipe(autoprefix())
        .pipe(concat('style.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream())

});

gulp.task('clean', function () {
    return gulp.src('./dist/**/*', {
        read: false
    })
        .pipe(clear())
});

gulp.task('script', function () {
    return gulp.src('src/js/**/*.js')
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(concat('script.min.js'))
        // .pipe(ugli())
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream())

});

gulp.task('img', function () {
    return gulp.src('src/img/*.png')
        .pipe(plumber())
        .pipe(imageMin())
        .pipe(gulp.dest('dist/img'))

});

gulp.task ('watch', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("src/sass/**/*.scss", gulp.series('sass'));
    gulp.watch("src/js/**/*.js", gulp.series('script'));
    gulp.watch("src/img/**/*", gulp.series('img'));
});

gulp.task('build', gulp.series('clean', 'sass', 'script', 'img'));
gulp.task('dev', gulp.series('sass', 'script', 'watch'));


